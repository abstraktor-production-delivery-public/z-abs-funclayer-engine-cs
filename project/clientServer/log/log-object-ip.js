
'use strict';


class LogObjectIp {
  constructor(actionId, stackId, stackName, secure, caption, local, remote) {
    this.actionId = actionId;
    this.stackId = stackId;
    this.stack = stackName;
    this.secure = secure;
    this.caption = caption;
    this.local = local;
    this.remote = remote;
  }
}


module.exports = LogObjectIp;
