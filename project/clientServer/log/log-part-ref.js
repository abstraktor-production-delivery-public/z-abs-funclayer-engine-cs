
'use strict';

const LogPartType = require('./log-part-type');


class LogPartRef {
  constructor(text, ref) {
    this.type = LogPartType.REF;
    this.text = text;
    this.ref = ref;
  }
}


module.exports = LogPartRef;
