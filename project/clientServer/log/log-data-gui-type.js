
'use strict';


class LogDataGuiType {
  static NEW_BROWSER = 0;
  static USE_BROWSER = 1;
  static NEW_PAGE = 2;
  static USE_PAGE = 3;
  static ACTION_CLICK = 4;
  static ACTION_TYPE = 5;
  static FUNCTION_CALL = 6;
  
  static TEXT_NEW_BROWSER = 'new browser';
  static TEXT_USE_BROWSER = 'use browser';
  static TEXT_NEW_PAGE = 'new page';
  static TEXT_USE_PAGE = 'use page';
  static TEXT_ACTION_CLICK = 'click';
  static TEXT_ACTION_TYPE = 'type';
  static TEXT_FUNCTION_CALL = 'function';
  
  static results = [
    LogDataGuiType.TEXT_NEW_BROWSER,
    LogDataGuiType.TEXT_USE_BROWSER,
    LogDataGuiType.TEXT_NEW_PAGE,
    LogDataGuiType.TEXT_USE_PAGE,
    LogDataGuiType.TEXT_ACTION_CLICK,
    LogDataGuiType.TEXT_ACTION_TYPE,
    LogDataGuiType.TEXT_FUNCTION_CALL
  ];

  static resultClasses = [
    'gui_seq_dia_browser_svg',
    'gui_seq_dia_browser_svg',
    'gui_seq_dia_page_svg',
    'gui_seq_dia_page_svg',
    'gui_seq_dia_click_svg',
    'gui_seq_dia_type_svg',
    'gui_seq_dia_not_impl_svg'
  ];
}


module.exports = LogDataGuiType;
