
'use strict';

const LogPartType = require('./log-part-type');


class LogPartError {
  constructor(text) {
    this.type = LogPartType.ERROR;
    this.text = text;
  }
}


module.exports = LogPartError;
