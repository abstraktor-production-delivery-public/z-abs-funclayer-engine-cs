
'use strict';


class LogPartType {
  static TEXT = 0;
  static REF = 1;
  static BIN = 2;
  static VALUE = 3;
  static ERROR = 4;
  static BUFFER = 5;
  static BUFFER_CACHED = 6;
}


module.exports = LogPartType;
