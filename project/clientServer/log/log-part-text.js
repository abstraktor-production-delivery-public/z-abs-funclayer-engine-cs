
'use strict';

const LogPartType = require('./log-part-type');


class LogPartText {
  constructor(text) {
    this.type = LogPartType.TEXT
    this.text = text;
  }
}


module.exports = LogPartText;
