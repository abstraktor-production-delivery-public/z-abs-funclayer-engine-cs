
'use strict';

const LogPartText = require('./log-part-text');


class LogInner {
  static TYPE_PARTS = 0;
  static TYPE_BUFFER = 1;
  static TYPE_BUFFER_INNER = 2;

  constructor(logParts, inner, open) {
    if(Array.isArray(logParts)) {
      this.logParts = logParts;
    }
    else {
      if('string' === typeof logParts) {
        this.logParts = [new LogPartText(logParts)];
      }
      else {
        this.logParts = [logParts];
      }
    }
    if(inner) {
      if(Array.isArray(inner)) {
        this.inners = inner;
      }
      else {
        this.inners = [inner];
      }
    }
    else {
      this.inners = [];
    }
    this.open = open ? true : false;
    this.type = LogInner.TYPE_PARTS;
    this.generatedHeight = 0;
    this.generatedWidth = 0;
  }
  
  add(inner) {
    if(Array.isArray(inner)) {
      this.inners = this.inners.concat(inner);
    }
    else {
      this.inners.push(inner);
      return inner;
    }
  }
  
  setType(type) {
    this.type = type;
  }
}


module.exports = LogInner;
