
'use strict';

let id = -1;
class LogDataAction {
  static STARTING = ++id;
  static STARTED = ++id;
  static ATTACHED = ++id;
  static STOPPING = ++id;
  static STOPPED = ++id;
  static DETACHED = ++id;
  static NOT_STARTED = ++id;
  static NOT_ATTACHED = ++id;
  static SEND = ++id;
  static RECEIVE = ++id;
  static CONNECTING = ++id;
  static CONNECTED = ++id;
  static DNS_FAILURE = ++id;
  static NOT_CONNECTED = ++id;
  static UPGRADING = ++id;
  static UPGRADED = ++id;
  static NOT_UPGRADED = ++id;
  static ACCEPTING = ++id;
  static ACCEPTED = ++id;
  static CLOSING = ++id;
  static CLOSED = ++id;
  static GUI = ++id;
  static STACK = ++id;

  static LOG = [
    'STARTING',
    'STARTED',
    'ATTACHED',
    'STOPPING',
    'STOPPED',
    'DETACHED',
    'NOT_STARTED',
    'NOT_ATTACHED',
    'SEND',
    'RECEIVE',
    'CONNECTING',
    'CONNECTED',
    'DNS_FAILURE',
    'NOT_CONNECTED',
    'UPGRADING',
    'UPGRADED',
    'NOT_UPGRADED',
    'ACCEPTING',
    'ACCEPTED',
    'CLOSING',
    'CLOSED',
    'GUI',
    'STACK'
  ];

  static LOG_SIGN = [
    '\u25B7',
    '\u25B6',
    '\u25B6',
    '\u25FB',
    '\u25FC',
    '\u25FC',
    '4',
    '4',
    '\u21D2\u2709',
    '\u21D0\u2709',
    '\u25CB\u2192\u25CB',
    '\u25CF\u2192\u25CF',
    'Dns',
    '9',
    'Sec',
    '\u24C8ec',
    '!Sec',
    '\u25CB',
    '\u25CF',
    '\u2613',
    '\u2612',
    '15',
    '16'
  ];
}


module.exports = LogDataAction;
