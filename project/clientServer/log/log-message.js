
'use strict';


class LogMessage {
  constructor(message, inners, data, dataBuffers) {
    this.message = 'string' === typeof message ? message : message();
    this.inners = inners;
    this.data = data;
    this.dataBuffers = dataBuffers;
  }
}


module.exports = LogMessage;

