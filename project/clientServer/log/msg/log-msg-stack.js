
'use strict';

const LogDataAction = require('../log-data-action');


class LogMsgStack {
  constructor(columnIndex, logDataStackType, stackId, stackType, stack) {
    this.actionId = LogDataAction.STACK;
    this.actorIndex = columnIndex;
    this.type = logDataStackType;
    this.stackId = stackId;
    this.stackType = stackType;
    this.stack = stack;
  }
}


module.exports = LogMsgStack;
