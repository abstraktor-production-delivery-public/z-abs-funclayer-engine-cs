
'use strict';


class LogMsgClient {
  constructor(actionId, stackName, local, remote, reverse) {
    this.actionId = actionId;
    this.stack = stackName;
    this.local = local;
    this.remote = remote;
    this.reverse = reverse;
  }
}


module.exports = LogMsgClient;
