
'use strict';


class LogMsgMessage {
  constructor(actionId, stackName, local, remote, caption) {
    this.actionId = actionId;
    this.stack = stackName;
    this.local = local;
    this.remote = remote;
    this.caption = caption;
  }
}


module.exports = LogMsgMessage;
