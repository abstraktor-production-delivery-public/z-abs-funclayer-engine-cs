
'use strict';

const LogDataAction = require('../log-data-action');


class LogMsgServer {
  constructor(actionId, stackName, local) {
    this.actionId = actionId;
    this.stack = stackName;
    this.local = local;
  }
}


module.exports = LogMsgServer;
