
'use strict';

const LogPartType = require('./log-part-type');


class LogPartBuffer {
  constructor(contentType, contentEncoding, transferEncoding) {
    this.type = LogPartType.BUFFER;
    this.contentType = contentType;
    this.contentEncoding = contentEncoding;
    this.transferEncoding = transferEncoding;
  }
}

module.exports = LogPartBuffer;
