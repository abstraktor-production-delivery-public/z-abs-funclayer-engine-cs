
'use strict';


class LogDataGuiSubType {
  static SUB_TYPE_OBJECT = 0;
  static SUB_TYPE_ACTION = 1;
  static SUB_TYPE_FUNCTION = 2;
    
  static subTypes = [
    LogDataGuiSubType.SUB_TYPE_OBJECT,
    LogDataGuiSubType.SUB_TYPE_OBJECT,
    LogDataGuiSubType.SUB_TYPE_OBJECT,
    LogDataGuiSubType.SUB_TYPE_OBJECT,
    LogDataGuiSubType.SUB_TYPE_ACTION,
    LogDataGuiSubType.SUB_TYPE_ACTION,
    LogDataGuiSubType.SUB_TYPE_FUNCTION
  ];
  
  static SUB_TYPE_TEXT_OBJECT = 'object';
  static SUB_TYPE_TEXT_ACTION = 'action';
  static SUB_TYPE_TEXT_FUNCTION = 'function';

  static subTypeNames = [
    LogDataGuiSubType.SUB_TYPE_TEXT_OBJECT,
    LogDataGuiSubType.SUB_TYPE_TEXT_OBJECT,
    LogDataGuiSubType.SUB_TYPE_TEXT_OBJECT,
    LogDataGuiSubType.SUB_TYPE_TEXT_OBJECT,
    LogDataGuiSubType.SUB_TYPE_TEXT_ACTION,
    LogDataGuiSubType.SUB_TYPE_TEXT_ACTION,
    LogDataGuiSubType.SUB_TYPE_TEXT_FUNCTION
  ];
}


module.exports = LogDataGuiSubType;
