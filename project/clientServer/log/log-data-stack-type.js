
'use strict';


class LogDataStackType {
  static NEW = 0;
  static SHARED = 1;
  static USE = 2;
  static STORE = 3;
  static DEL = 4;

  static results = [
    'new stack',
    'shared stack',
    'use stack',
    'store stack',
    'del stack'
  ];

  static templateWidths = [
    50,
    60,
    47,
    55,
    45
  ];
}


module.exports = LogDataStackType;
