
'use strict';

const LogPartType = require('./log-part-type');


class LogPartBufferCached {
  constructor(url) {
    this.type = LogPartType.BUFFER_CACHED;
    this.url = url;
  }
}


module.exports = LogPartBufferCached;
