
'use strict';


let id = -1;
class LogSourceType {
  static UNKNOWN = ++id;
  static SERVER = ++id;
  static STACK = ++id;
  static ACTOR = ++id;
}


module.exports = LogSourceType;
