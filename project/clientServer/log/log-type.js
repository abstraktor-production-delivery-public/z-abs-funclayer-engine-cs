
'use strict';


class LogType {
  // DOUBLE-CODE: z-abs-corelayer-cs
  static ENGINE = 0;
  static DEBUG = 1;
  static ERROR = 2;
  static WARNING = 3;
  static IP = 4;
  static GUI = 5;
  static VERIFY_SUCCESS = 6;
  static VERIFY_FAILURE = 7;
  static TEST_DATA = 8;
  static BROWSER_LOG = 9;
  static BROWSER_ERR = 10;

  static names = [
    'Engine  ',
    'Debug   ',
    'Error   ',
    'Warning ',
    'IP      ',
    'GUI     ',
    'Success ',
    'Failure ',
    'TestData',
    'B-Log   ',
    'B-Err   '
  ];

  static pureNames = [
    'Engine',
    'Debug',
    'Error',
    'Warning',
    'IP',
    'GUI',
    'Success',
    'Failure',
    'TestData',
    'BrowserLog',
    'BrowserErr'
  ];

  static csses = [
    'log_engine',
    'log_debug',
    'log_error',
    'log_warning',
    'log_ip',
    'log_gui',
    'log_verify_success',
    'log_verify_failure',
    'log_test_data',
    'log_browser_log',
    'log_browser_err'
  ];

  static getClassFromName(resultName) {
    const foundIndex = LogType.pureNames.indexOf(resultName);
    if(-1 !== foundIndex) {
      return LogType.csses[foundIndex];
    }
    else {
      return '';
    }
  }
}


module.exports = LogType;
