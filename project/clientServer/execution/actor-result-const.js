
'use strict';


class ActorResultConst {
  static NONE = 0;
  static NA = 1;
  static N_IMPL = 2;
  static E_INTERRUPT = 3;
  static SUCCESS = 4;
  static E_FAILURE = 5;
  static N_EXEC = 6;
  static INTERRUPT = 7;
  static FAILURE = 8;
  static ERROR = 9;
  static TIMEOUT = 10;
  static REFLECTION = 11;
  
  static TYPE_EXECUTED = 0;
  static TYPE_NOT_EXECUTED = 1;
  
  static TYPES = [
    ActorResultConst.TYPE_NOT_EXECUTED,
    ActorResultConst.TYPE_NOT_EXECUTED,
    ActorResultConst.TYPE_NOT_EXECUTED,
    ActorResultConst.TYPE_EXECUTED,
    ActorResultConst.TYPE_EXECUTED,
    ActorResultConst.TYPE_EXECUTED,
    ActorResultConst.TYPE_NOT_EXECUTED,
    ActorResultConst.TYPE_EXECUTED,
    ActorResultConst.TYPE_EXECUTED,
    ActorResultConst.TYPE_EXECUTED,
    ActorResultConst.TYPE_EXECUTED,
    ActorResultConst.TYPE_EXECUTED
  ];
  
  
  static results = [
    'None',
    'n/a',
    'n_impl',
    'e_interrupt',
    'Success',
    'e_failure',
    'n_exec',
    'Interrupt',
    'Failure',
    'Error',
    'Timeout',
    'Reflection'
  ];

  static tableNameResults = [
    'none',
    'n/a',
    'n_impl',
    'e_interrupt',
    'success',
    'e_failure',
    'n_exec',
    'interrupt',
    'failure',
    'error',
    'timeout',
    'reflection'
  ];
  
  static resultIds = [ // TODO: Remove this silly constant
    0,
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10,
    11
  ];
  
  static templateWidths = [
    44,
    17,
    35,
    56,
    43,
    44,
    36,
    44,
    44,
    35,
    44,
    35
  ];
  
  static classes = [
    'test_none',
    'test_na',
    'test_nimpl',
    'test_e_interrupt',
    'test_success',
    'test_e_failure',
    'test_nexec',
    'test_interrupt',
    'test_failure',
    'test_error',
    'test_timeout',
    'test_reflection'
  ];
  
  static classesLight = [
    'test_none-light',
    'test_na-light',
    'test_nimpl-light',
    'test_e_interrupt-light',
    'test_success-light',
    'test_e_failure-light',
    'test_nexec-light',
    'test_interrupt-light',
    'test_failure-light',
    'test_error-light',
    'test_timeout-light',
    'test_reflection-light'
  ];
  
  // TODO: REMOVE
  static resultClasses = [
    'test_none_svg',
    'test_na_svg',
    'test_nimpl_svg',
    'test_e_interrupt_svg',
    'test_success_svg',
    'test_e_failure_svg',
    'test_nexec_svg',
    'test_interrupt_svg',
    'test_failure_svg',
    'test_error_svg',
    'test_timeout_svg',
    'test_reflection_svg'
  ];
  
  static stateTypeClasses = [
    'state_not_executed',
    'state_not_executed',
    'state_not_executed',
    'state_not_executed',
    'state_not_executed',
    'state_not_executed',
    'state_executed',
    'state_executed',
    'state_executed',
    'state_executed',
    'state_executed',
    'state_executed'
  ];
  
  static RESULTS_CONSOLE = [
    ddb.white('NONE       '),
    ddb.grey('NA         '),
    ddb.grey('N_IMPL     '),
    ddb.green('E_INTERRUPT'),
    ddb.greenBright('SUCCESS    '),
    ddb.green('E_FAILURE  '),
    ddb.grey('N_EXEC     '),
    ddb.yellow('INTERRUPT  '),
    ddb.redBright('FAILURE    '),
    ddb.red('ERROR      '),
    ddb.blue('TIMEOUT    '),
    ddb.yellow('REFLECTION ')
  ];
  
  static _ActorResultConst_tableNameResultIds = new Map();
  static _ActorResultConst_tableNameClasses = new Map();

  static getTableNameResultIds(tableNameResult, emptyIsSuccess = false) {
    if(emptyIsSuccess && '' === tableNameResult) {
      return ActorResultConst.SUCCESS;
    }
    else {
      return ActorResultConst._ActorResultConst_tableNameResultIds.get(tableNameResult);
    }
  }
  
  static getClassFromResult(result) {
    return ActorResultConst._ActorResultConst_tableNameClasses.get(result);
  }
  
  static getName(resultId) {
    return ActorResultConst.results[resultId];
  }
  
  static getClass(resultId) {
    return ActorResultConst.classes[resultId];
  }
  
  static getClassLight(resultId) {
    return ActorResultConst.classesLight[resultId];
  }
}

for(let i = 0; i < ActorResultConst.tableNameResults.length; ++i) {
  ActorResultConst._ActorResultConst_tableNameResultIds.set(ActorResultConst.tableNameResults[i], i);
  ActorResultConst._ActorResultConst_tableNameClasses.set(ActorResultConst.tableNameResults[i], ActorResultConst.classes[i]);
}


module.exports = ActorResultConst;
