
'use strict';


class ActorStateConst {
  static DATA = 0;
  static INIT_SERVER = 1;
  static INIT_CLIENT = 2;
  static RUN = 3;
  static EXIT = 4;
  static NONE = 5;

  static actorStateNames = [
    'data',
    'initServer',
    'initClient',
    'run',
    'exit',
    'none'
  ];
  
  static getName(actorStateId) {
    return ActorStateConst.actorStateNames[actorStateId];
  }
}


module.exports = ActorStateConst;
