
'use strict';


class ActorPhaseConst {
  static DATA = 0; // Implicit phase when all Actors gets their data.
  static PRE = 1;
  static EXEC = 2;
  static POST = 3;
  static PRE_POST = 4;
  static ALL = 5;
  static NONE = 4;

  static NAME_DATA = 'data';
  static NAME_PRE = 'pre';
  static NAME_EXEC = 'exec';
  static NAME_POST = 'post';
  static NAME_PRE_POST = 'pre, post';
  static NAME_ALL = 'all';
  static NAME_NONE = 'none';

  static names = [
    ActorPhaseConst.NAME_DATA,
    ActorPhaseConst.NAME_PRE,
    ActorPhaseConst.NAME_EXEC,
    ActorPhaseConst.NAME_POST,
    ActorPhaseConst.NAME_PRE_POST,
    ActorPhaseConst.NAME_ALL
  ];

  static namesRunning = [
    ActorPhaseConst.NAME_DATA,
    ActorPhaseConst.NAME_PRE,
    ActorPhaseConst.NAME_EXEC,
    ActorPhaseConst.NAME_POST,
    ActorPhaseConst.NAME_NONE
  ];

  static ids = new Map(
    [
      [ActorPhaseConst.NAME_DATA, ActorPhaseConst.DATA],
      [ActorPhaseConst.NAME_PRE, ActorPhaseConst.PRE],
      [ActorPhaseConst.NAME_EXEC, ActorPhaseConst.EXEC],
      [ActorPhaseConst.NAME_POST, ActorPhaseConst.POST],
      [ActorPhaseConst.NAME_PRE_POST, ActorPhaseConst.PRE_POST],
      [ActorPhaseConst.NAME_ALL, ActorPhaseConst.ALL],
      [ActorPhaseConst.NAME_NONE, ActorPhaseConst.NONE]
    ]
  );

static getName(id) {
    return ActorPhaseConst.names[id];
  }
  
  static getId(name) {
    return ActorPhaseConst.ids.get(name);
  }
}


module.exports = ActorPhaseConst;
