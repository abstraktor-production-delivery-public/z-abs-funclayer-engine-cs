	
'use strict';


class ActorTypeConst {
  static LOCAL = 0;
  static COND = 1;
  static ORIG = 2;
  static REAL_SUT = 3;
  static SUT = 4;
  static PROXY = 5;
  static TERM = 6;
  static INTER = 7;
  
  static NAME_LOCAL = 'local';
  static NAME_COND = 'cond';
  static NAME_ORIG = 'orig';
  static NAME_REAL_SUT = 'real_sut';
  static NAME_SUT = 'sut';
  static NAME_PROXY = 'proxy';
  static NAME_TERM = 'term';
  static NAME_INTER = 'inter';
  
  static FULL_NAME_LOCAL = 'Local';
  static FULL_NAME_COND = 'Condition';
  static FULL_NAME_ORIG = 'Originating';
  static FULL_NAME_REAL_SUT = 'RealSut';
  static FULL_NAME_SUT = 'Sut';
  static FULL_NAME_PROXY = 'Proxy';
  static FULL_NAME_TERM = 'Terminating';
  static FULL_NAME_INTER = 'Intercepting';
  
  static names = [
    ActorTypeConst.NAME_LOCAL,
    ActorTypeConst.NAME_COND,
    ActorTypeConst.NAME_ORIG,
    ActorTypeConst.NAME_REAL_SUT,
    ActorTypeConst.NAME_SUT,
    ActorTypeConst.NAME_PROXY,
    ActorTypeConst.NAME_TERM,
    ActorTypeConst.NAME_INTER
  ];
  
  static ids = new Map(
    [
      [ActorTypeConst.NAME_LOCAL, ActorTypeConst.LOCAL],
      [ActorTypeConst.NAME_COND, ActorTypeConst.COND],
      [ActorTypeConst.NAME_ORIG, ActorTypeConst.ORIG],
      [ActorTypeConst.NAME_REAL_SUT, ActorTypeConst.REAL_SUT],
      [ActorTypeConst.NAME_SUT, ActorTypeConst.SUT],
      [ActorTypeConst.NAME_PROXY, ActorTypeConst.PROXY],
      [ActorTypeConst.NAME_TERM, ActorTypeConst.TERM],
      [ActorTypeConst.NAME_INTER, ActorTypeConst.INTER]
    ]
  );
  
  static fullNameIds = new Map(
    [
      [ActorTypeConst.FULL_NAME_LOCAL, ActorTypeConst.LOCAL],
      [ActorTypeConst.FULL_NAME_COND, ActorTypeConst.COND],
      [ActorTypeConst.FULL_NAME_ORIG, ActorTypeConst.ORIG],
      [ActorTypeConst.FULL_NAME_REAL_SUT, ActorTypeConst.REAL_SUT],
      [ActorTypeConst.FULL_NAME_SUT, ActorTypeConst.SUT],
      [ActorTypeConst.FULL_NAME_PROXY, ActorTypeConst.PROXY],
      [ActorTypeConst.FULL_NAME_TERM, ActorTypeConst.TERM],
      [ActorTypeConst.FULL_NAME_INTER, ActorTypeConst.INTER]
    ]
  );

  static getName(id) {
    return ActorTypeConst.names[id];
  }
  
  static getId(name) {
    return ActorTypeConst.ids.get(name);
  }
  
  static getPriority(id) {
    
  }
}


module.exports = ActorTypeConst;
