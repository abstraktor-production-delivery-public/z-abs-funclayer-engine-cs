
'use strict';

const AppProtocolConst = require('../../app-protocol/app-protocol-const');


class MessageTestCaseDebugStepOver {
  constructor(sessionId) {
    this.msgId = AppProtocolConst.TEST_CASE_DEBUG_STEP_OVER;
    this.sessionId = sessionId;
  }
}


module.exports = MessageTestCaseDebugStepOver;
