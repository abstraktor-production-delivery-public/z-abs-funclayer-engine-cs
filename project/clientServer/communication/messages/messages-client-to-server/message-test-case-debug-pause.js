
'use strict';

const AppProtocolConst = require('../../app-protocol/app-protocol-const');


class MessageTestCaseDebugPause {
  constructor(sessionId) {
    this.msgId = AppProtocolConst.TEST_CASE_DEBUG_PAUSE;
    this.sessionId = sessionId;
  }
}


module.exports = MessageTestCaseDebugPause;
