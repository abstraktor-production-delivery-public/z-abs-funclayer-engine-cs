
'use strict';

const AppProtocolConst = require('../../app-protocol/app-protocol-const');


class MessageTestCaseDebugContinue {
  constructor(sessionId) {
    this.msgId = AppProtocolConst.TEST_CASE_DEBUG_CONTINUE;
    this.sessionId = sessionId;
  }
}


module.exports = MessageTestCaseDebugContinue;
