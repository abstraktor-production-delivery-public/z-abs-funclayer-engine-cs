
'use strict';

const AppProtocolConst = require('../../app-protocol/app-protocol-const');


class MessageTestCaseStop {
  constructor(sessionId) {
    this.msgId = AppProtocolConst.TEST_CASE_STOP;
    this.sessionId = sessionId;
  }
}


module.exports = MessageTestCaseStop;
