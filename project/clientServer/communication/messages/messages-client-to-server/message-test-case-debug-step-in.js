
'use strict';

const AppProtocolConst = require('../../app-protocol/app-protocol-const');


class MessageTestCaseDebugStepIn {
  constructor(sessionId) {
    this.msgId = AppProtocolConst.TEST_CASE_DEBUG_STEP_IN;
    this.sessionId = sessionId;
  }
}


module.exports = MessageTestCaseDebugStepIn;
