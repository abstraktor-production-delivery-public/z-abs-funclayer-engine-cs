
'use strict';

const AppProtocolConst = require('../../app-protocol/app-protocol-const');


class MessageTestCaseDebugStepOut {
  constructor(sessionId) {
    this.msgId = AppProtocolConst.TEST_CASE_DEBUG_STEP_OUT;
    this.sessionId = sessionId;
  }
}


module.exports = MessageTestCaseDebugStepOut;
