
'use strict';

const AppProtocolConst = require('../../app-protocol/app-protocol-const');


class MessageTestSuiteStop {
  constructor(sessionId) {
    this.msgId = AppProtocolConst.TEST_SUITE_STOP;
    this.sessionId = sessionId;
  }
}


module.exports = MessageTestSuiteStop;
